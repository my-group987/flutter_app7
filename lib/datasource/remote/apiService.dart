import 'dart:convert';
import 'package:flutter_application_7/model/dataresponse.dart';
import 'package:flutter_application_7/presentations/constant.dart';
import 'package:http/http.dart' as http;

class ApiService {
  ApiService._();
  static final ApiService service = ApiService._();
  Future<DataResponse> fetchData() async {
    String url = '$baseUrl/3/movie/popular?api_key=$apiKey';
    http.Response response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      var jsonBody = jsonDecode(response.body);
      return DataResponse.fromJson(jsonBody);
    } else {
      throw Exception('unable to fetch data');
    }
  }
}
