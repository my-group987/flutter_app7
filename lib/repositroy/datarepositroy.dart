import 'package:flutter_application_7/datasource/remote/apiService.dart';
import 'package:flutter_application_7/model/dataresponse.dart';

class DataRepository {
  Future<DataResponse> fetchRemoteData() {
   return ApiService.service.fetchData();
  }
}
